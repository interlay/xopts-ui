import React, { Component } from "react";
import { ListGroup, ListGroupItem, Form, FormGroup, FormControl, Modal, Badge, Col, Container, Row, Alert } from "react-bootstrap";
import * as utils from '../../utils/utils';
import { showSuccessToast, showFailureToast } from '../../controllers/toast';
import { SpinButton } from '../SpinButton';
import { AppProps } from "../../types/App";
import { Big } from 'big.js';
import { FormControlElement } from "../../types/Inputs";
import { OptionInterface } from "../../types/Contracts";
import { decodeAddress } from "../../utils/address";

interface EnterAmountProps extends SellWizardState {
  handleChange: (event: React.ChangeEvent<FormControlElement>) => void,
}

interface EnterAmountState {
  amountDai: number
}

class EnterAmount extends Component<EnterAmountProps, EnterAmountState> {
  state: EnterAmountState = {
    amountDai: 0,
  }

  constructor(props: EnterAmountProps) {
    super(props);
    this.state = {
      amountDai: parseInt(this.props.amountDai.toString()),
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event: React.ChangeEvent<FormControlElement>) {
    let {name, value} = event.target
    this.setState(state => ({
      ...state,
      [name]: value
    }));
    this.props.handleChange(event);
  }

  render() {
    if (this.props.currentStep !== 1) {
      return null
    }
    const { errors } = this.props;
    return(
      <FormGroup>
        <FormGroup>
          <Form.Label>How much DAI do you want to underwrite (insurance collateral)?</Form.Label>
          <FormControl
            id="amountDai"
            name="amountDai"
            type="number"
            value={this.props.amountDai.toString()}
            isInvalid={this.state.amountDai <= 0}
            onChange={this.handleChange}
          />
        <FormControl.Feedback type="invalid">
          {errors?.amountDai}
        </FormControl.Feedback>

        </FormGroup>
        <FormGroup>
          <Form.Label>Current Balance</Form.Label>
          <FormControl type="text" placeholder={this.props.balance.toString() + " DAI"} readOnly disabled/>
        </FormGroup>
      </FormGroup>
    )
  }
}

interface EnterAddressProps extends SellWizardState {
  handleChange: (event: React.ChangeEvent<FormControlElement>) => void,
}

class EnterAddress extends Component<EnterAddressProps> {
  render() {
    if (this.props.currentStep !== 2) {
      return null
    }
    const { errors } = this.props;
    return(
      <FormGroup>
        <h5>Enter your BTC Address</h5>
        <FormControl
          id="btcAddress"
          name="btcAddress"
          type="text"
          placeholder="BTC Address"
          value={this.props.btcAddress || ''}
          onChange={this.props.handleChange}
          isInvalid={errors.btcAddress ? true : false}
          required
        />
        <FormControl.Feedback type="invalid">
          {errors?.btcAddress}
        </FormControl.Feedback>
      </FormGroup>
    )
  }
}

interface ConfirmProps {
  currentStep: number
  strikePrice: Big
  expiry: number
  amountDai: Big
  btcAddress: string
  spinner: boolean
}

class Confirm extends Component<ConfirmProps> {
  render() {
    if (this.props.currentStep !== 3) {
      return null
    }
    return(
      <FormGroup>
        <h5>Confirm & Pay</h5>
        <FormGroup>
          <ListGroup>
              <ListGroupItem>Strike Price: <strong>{this.props.strikePrice.toString()} DAI</strong></ListGroupItem>
              <ListGroupItem>Expiry: <strong>{new Date(this.props.expiry*1000).toLocaleString()}</strong></ListGroupItem>
              <ListGroupItem>Amount: <strong>{this.props.amountDai.toString()} DAI -&gt; {this.props.amountDai.toString()} XOPT</strong></ListGroupItem>
              <ListGroupItem>Underwrites: <strong>{utils.calculateAvailableBTC(this.props.amountDai, this.props.strikePrice).round(2).toString()} BTC</strong></ListGroupItem>
              <ListGroupItem>BTC Address: <strong>{this.props.btcAddress}</strong></ListGroupItem>
          </ListGroup>
          <Alert variant="danger" className="mt-3">
            You will <strong>not</strong> be able to withdraw your DAI until the option expires.
          </Alert>
        </FormGroup>
        <SpinButton spinner={this.props.spinner}/>
      </FormGroup>
    )
  }
}

interface SellWizardProps extends AppProps {
  contract: string
  toast: any
  hideModal: () => void
  showModal: boolean
}

interface SellWizardState {
  contractAddress: string
  currentStep: number
  amountDai: Big
  btcAddress: string
  optionContract?: OptionInterface
  spinner: boolean
  expiry: number
  strikePrice: Big
  balance: Big
  errors: {
    amountDai?: string
    btcAddress?: string
  }
}

export default class SellWizard extends Component<SellWizardProps> {
  state: SellWizardState = {
    contractAddress: this.props.contract,
    currentStep: 1,
    amountDai: utils.newBig(1),
    btcAddress: '',
    spinner: false,
    expiry: 0,
    strikePrice: utils.newBig(0),
    balance: utils.newBig(0),
    errors: {},
  }

  constructor(props: SellWizardProps) {
    super(props)
    this._next = this._next.bind(this)
    this._prev = this._prev.bind(this)

    this.handleChange = this.handleChange.bind(this)
  }

  async componentDidMount() {
    if (this.props.contracts) {
      let balance = await this.props.contracts.balanceOf();
      this.setState({
        balance: utils.weiDaiToDai(utils.newBig(balance.toString())),
      });
    }
  }

  async componentDidUpdate() {
    if (this.state.contractAddress !== this.props.contract) {
      const contract = this.props.contract;

      let contracts = this.props.contracts;
      let optionContract = contracts.attachOption(contract);
      let {expiry, strikePrice} = await optionContract.getDetails();

      this.setState({
        contractAddress: contract,
        optionContract: optionContract,
        expiry: parseInt(expiry.toString()),
        strikePrice: utils.weiDaiToBtc(utils.newBig(strikePrice.toString())),
      });
    }
  }

  handleChange(event: React.ChangeEvent<FormControlElement>) {
    let {name, value} = event.target
    if (name === "amountDai") {
      const amountDai = utils.newBig(value || 0);
      this.setState({
        amountDai: amountDai,
        errors: {
          amountDai: amountDai.gt(0) ? undefined : "Invalid amount",
        }
      });
    } else if (name === "btcAddress") {
      this.setState({
        btcAddress: value,
        errors: {
          btcAddress: decodeAddress(value) ? undefined : "Invalid address",
        }
      });
    } else {
      this.setState({
        [name]: value
      });
    }
  }

  isValid(step: number) {
    const { amountDai, btcAddress } = this.state;
    let valid = [
      amountDai.gt(0),
      decodeAddress(btcAddress) ? true : false,
      true,
    ];
    return valid[step];
  }

  handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    let currentStep = this.state.currentStep;
    if (currentStep <= 2) {
      if (!this.isValid(currentStep-1)) return;
      this.setState({currentStep: currentStep + 1});
      return;
    }

    const { amountDai, btcAddress, optionContract } = this.state;

    try {
      let dai = await this.props.contracts.balanceOf();
      if (amountDai.gt(dai.toString())) {
        showFailureToast(this.props.toast, 'Insufficient collateral!', 3000);
        return;
      }
    } catch(err) {
      showFailureToast(this.props.toast, 'Something went wrong...', 3000);
    }

    this.setState({spinner: true});
    try {
      let contracts = this.props.contracts;
      let weiDai = utils.daiToWeiDai(amountDai);
      await contracts.checkAllowance(weiDai);
      if (optionContract) {
        await contracts.underwriteOption(optionContract.address, weiDai, btcAddress);
        //this.props.history.push("/positions")
        showSuccessToast(this.props.toast, 'Successfully sold options!', 3000);
        this.exitModal();
      } else {
        throw Error("Options contract not found.");
      }
    } catch(error) {
      console.log(error);
      showFailureToast(this.props.toast, 'Failed to send transaction...', 3000);
      this.setState({spinner: false});
    }
  }

  _next() {
    let currentStep = this.state.currentStep;
    if (!this.isValid(currentStep-1)) return;
    // If the current step is 1 or 2, then add one on "next" button click
    currentStep = currentStep >= 2? 3: currentStep + 1;
    this.setState({
      currentStep: currentStep
    });
  }

  _prev() {
    let currentStep = this.state.currentStep
    // If the current step is 2 or 3, then subtract one on "previous" button click
    currentStep = currentStep <= 1? 1: currentStep - 1
    this.setState({
      currentStep: currentStep
    })
  }

  get previousButton(){
    let currentStep = this.state.currentStep;
    // If the current step is not 1, then render the "previous" button
    if(currentStep!==1){
      return (
        <button
          className="btn btn-secondary float-left"
          type="button" onClick={this._prev}>
        Previous
        </button>
      )
    }
    // ...else return nothing
    return null;
  }

  get nextButton(){
    let currentStep = this.state.currentStep;
    // If the current step is not 3, then render the "next" button
    if(currentStep<3){
      return (
        <button
          className="btn btn-primary float-right"
          type="button" onClick={this._next}>
        Next
        </button>
      )
    }
    // ...else render nothing
    return null;
  }

  exitModal() {
    this.props.hideModal();
    this.setState({currentStep: 1});
  }

  render() {
    return (
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={this.props.showModal} onHide={() => this.exitModal()}>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
              Sell Options
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={this.handleSubmit}>
            <EnterAmount
              handleChange={this.handleChange}
              {...this.state}
            />
            <EnterAddress
              handleChange={this.handleChange}
              {...this.state}
            />
            <Confirm
              currentStep={this.state.currentStep}
              amountDai={this.state.amountDai}
              btcAddress={this.state.btcAddress}
              spinner={this.state.spinner}
              expiry={this.state.expiry}
              strikePrice={this.state.strikePrice}
            />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {this.previousButton}
          {this.nextButton}
        </Modal.Footer>
      </Modal>
    )
  }
}